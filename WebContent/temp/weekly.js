{
  "response": {
    "version": "0.1",
    "day": [
      {
        "date": "Week Ending on April 5",
        "teaching": "A successful human being is a blessing to the society because he very naturally wants to contribute.",
        "image": "images/general-nature/1s.jpg",
        "audiourl": "https://dl.dropboxusercontent.com/s/16hjgu63ops5hw2/webcast_teaching_91_06-10-2012.mp3",
        "author": "Sri Bhagavathi Bhagavan"
      },
      {
        "date": "Week Ending on April 12",
        "teaching": "Being successful is helpful not only at the individual level but also at the collective level.",
        "image": "images/general-nature/1s.jpg",
        "audiourl": "https://dl.dropboxusercontent.com/s/16hjgu63ops5hw2/webcast_teaching_91_06-10-2012.mp3",
        "author": "Sri Bhagavathi Bhagavan",
        "position": "left"
      },
      {
        "date": "Week Ending on April 19",
        "teaching": "A successful person creates a happy family, a happy work place and a happy society.",
        "image": "images/general-nature/1s.jpg",
        "audiourl": "https://dl.dropboxusercontent.com/s/16hjgu63ops5hw2/webcast_teaching_91_06-10-2012.mp3",
        "author": "Sri Bhagavathi Bhagavan"
      },
      {
        "date": "Week Ending on April 26",
        "teaching": "Education helps you to learn the art of living.",
        "image": "images/general-nature/1s.jpg",
        "audiourl": "https://dl.dropboxusercontent.com/s/16hjgu63ops5hw2/webcast_teaching_91_06-10-2012.mp3",
        "author": "Sri Bhagavathi Bhagavan",
        "position": "left"
      }
    ]
  }
}