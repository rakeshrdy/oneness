{
  "response": {
    "version": "0.1",
    "day": [
      {
        "date": "26 Apr 2014",
        "teaching": "A successful human being is a blessing to the society because he very naturally wants to contribute.",
        "image": "images/general-nature/1s.jpg",
        "author": "Sri Bhagavan"
      },
      {
        "date": "25 Apr 2014",
        "teaching": "Being successful is helpful not only at the individual level but also at the collective level.",
        "image": "images/general-nature/1s.jpg",
        "author": "Sri Bhagavan",
        "position": "left"
      },
      {
        "date": "24 Apr 2014",
        "teaching": "A successful person creates a happy family, a happy work place and a happy society.",
        "image": "images/general-nature/1s.jpg",
        "author": "Sri Bhagavan"
      },
      {
        "date": "23 Apr 2014",
        "teaching": "Education helps you to learn the art of living.",
        "image": "images/general-nature/1s.jpg",
        "author": "Sri Bhagavan",
        "position": "left"
      }
    ]
  }
}