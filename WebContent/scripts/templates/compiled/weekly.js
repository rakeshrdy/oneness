(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['weekly'] = template({"1":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", escapeExpression=this.escapeExpression, buffer = " \r\n            	<div class=\"one-half-responsive ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.position), {"name":"if","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " \">\r\n                    <p class=\" ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.position), {"name":"if","hash":{},"fn":this.program(4, data),"inverse":this.program(6, data),"data":data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += " no-bottom\">\r\n                      <!--  <img src=\""
    + escapeExpression(((helper = helpers.image || (depth0 && depth0.image)),(typeof helper === functionType ? helper.call(depth0, {"name":"image","hash":{},"data":data}) : helper)))
    + "\" alt=\"Unable to load image\">  -->\r\n                        <em>"
    + escapeExpression(((helper = helpers.date || (depth0 && depth0.date)),(typeof helper === functionType ? helper.call(depth0, {"name":"date","hash":{},"data":data}) : helper)))
    + " <br> </em>\r\n                        <em>"
    + escapeExpression(((helper = helpers.teaching || (depth0 && depth0.teaching)),(typeof helper === functionType ? helper.call(depth0, {"name":"teaching","hash":{},"data":data}) : helper)))
    + " </em>\r\n						<em><br>-"
    + escapeExpression(((helper = helpers.author || (depth0 && depth0.author)),(typeof helper === functionType ? helper.call(depth0, {"name":"author","hash":{},"data":data}) : helper)))
    + "</em>\r\n                    </p>\r\n                    <div class=\"thumb-clear\"></div>\r\n\r\n					<audio controls>\r\n  					<source src=\""
    + escapeExpression(((helper = helpers.audiourl || (depth0 && depth0.audiourl)),(typeof helper === functionType ? helper.call(depth0, {"name":"audiourl","hash":{},"data":data}) : helper)))
    + "\" type=\"audio/mpeg\">\r\n					Your browser does not support the audio element.\r\n					</audio>\r\n\r\n\r\n                </div>\r\n				<div class=\"decoration  ";
  stack1 = helpers['if'].call(depth0, (depth0 && depth0.position), {"name":"if","hash":{},"fn":this.program(8, data),"inverse":this.program(10, data),"data":data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer + "  \"></div>\r\n                ";
},"2":function(depth0,helpers,partials,data) {
  return " last-column ";
  },"4":function(depth0,helpers,partials,data) {
  return " thumb-right ";
  },"6":function(depth0,helpers,partials,data) {
  return " thumb-left ";
  },"8":function(depth0,helpers,partials,data) {
  return "  ";
  },"10":function(depth0,helpers,partials,data) {
  return " hide-if-responsive ";
  },"compiler":[5,">= 2.0.0"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, options, functionType="function", blockHelperMissing=helpers.blockHelperMissing, buffer = "";
  stack1 = ((helper = helpers.day || (depth0 && depth0.day)),(options={"name":"day","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data}),(typeof helper === functionType ? helper.call(depth0, options) : helper));
  if (!helpers.day) { stack1 = blockHelperMissing.call(depth0, stack1, options); }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer + " ";
},"useData":true});
})();