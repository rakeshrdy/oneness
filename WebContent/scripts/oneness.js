
var headerdata ={
	    version: "0.1",
	    sidebaritem: [
	      {
	        url: "index.html",
	        name: "Home",
	        classn: "home-icon",
	      },
	      {
		        url: "#",
		        name: "Teachings",
		        classn: "teaching_icon",
		        submenus: [
		           	      {
		          	        pageurls: "daily.html",
		          	        names: "Daily Teachings",
		          	      },
		          	      {
		          		        pageurls: "weekly.html",
		          		        names: "Weekly Teachings",
		          		  },
		          	      {
		          		        pageurls: "conferences.html",
		          		        names: "Conferences",
		          		  }]
		  },
		  {
		        url: "webcast.html",
		        name: "Webcasts",
		        classn: "web_icon",
		  },
		  {
		        url: "omwebcasts.html",
		        name: "Live OM Webcasts",
		        classn: "web_icon",
		  },
		  {
		        url: "experiences.html",
		        name: "Experiences",
		        classn: "web_icon",
		  },
		  {
		        url: "#",
		        name: "About Us",
		        classn: "about_icon",
		        submenus: [
		           	      {
		          	        pageurls: "founders.html",
		          	        names: "Founders",
		          	      },
		          	      {
		          		        pageurls: "temple.html",
		          		        names: "University",
		          		  },
		          	      {
		          		        pageurls: "deeksha.html",
		          		        names: "Deeksha",
		          		  },
		          	      {
		          		        pageurls: "vision.html",
		          		        names: "Vision",
		          		  }]
		  },
		  {
		        url: "programs.html",
		        name: "Programs",
		        classn: "program_icon",
		  },
		  {
		        url: "contact.html",
		        name: "Contact Us",
		        classn: "contact_icon",
		  }
		  
	    ]
	  }; 

var app = {
		
		renderSidebarView: function() {
			$( Handlebars.templates["sidebar"](headerdata)).insertAfter("#sidebarHolder");

			//var sidebarRawsource = $("#sidebarRawTemplate").html(); 
			//var sidebarCompiledTemplate = Handlebars.compile(sidebarRawsource); 
				/*$("#sidebarHolder").append(sidebarCompiledTemplate(headerdata));*/
			
			$('.submenu-deploy').click(function(){
				$(this).parent().find('.nav-submenu').toggle(100);
				$(this).parent().find('.sidebar-decoration').toggle(100);
				$(this).find('em').toggleClass('dropdown-item');
				return false;
			});
	    },
	    
	    renderWeeklyPageView: function() {
	    	if ( $( "#wtsHolder" ).length ) {
	    		wp.initializeHandleBars();
	    		db.weeklyTeachs(10);
	    	}
	    },

	    renderDailyPageView: function() {
	    	if ( $( "#dtsHolder" ).length ) {
	    		wp.initializeHandleBars();
	    		db.dailyTeachs(10);
	    	}
	    },
	    
	    renderConfPageView: function() {
	    	if ( $( "#confHolder" ).length ) {
	    		wp.initializeHandleBars();
	    		db.confTeachs(10);
	    	}
	    },
	    
	    renderExpPageView: function() {
	    	if ( $( "#expHolder" ).length ) {
	    		wp.initializeHandleBars();
	    		db.expTeachs(10);
	    	}
	    },
	    
	    renderSingleExpPageView: function(sno) {
	    	if ( $( "#expHolder" ).length ) {
	    		wp.initializeHandleBars();
	    		db.singleExpTeachs(sno);
	    	}
	    },
	    
	    initialize: function() {
	        var self = this;
	        self.renderSidebarView();
	        self.renderWeeklyPageView();
	        self.renderDailyPageView();
	        if(self.getParameterByName('objectId')){
	        	self.renderSingleExpPageView(self.getParameterByName('objectId'));
	        }else{
	        	self.renderExpPageView();
	        }
	        
	        if(self.getParameterByName('confId')){
	        	self.renderConfPageView(self.getParameterByName('confId'));
	        }else{
	        	self.renderConfPageView();
	        }
	        
	    },

	    showAlert: function (message, title) {
	        if (navigator.notification) {
	            navigator.notification.alert(message, null, title, 'OK');
	        } else {
	            alert(title ? (title + ": " + message) : message);
	        }
	    },
	    
	    getParameterByName:function(name) {
	        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	            results = regex.exec(location.search);
	        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	    }
	    
	};


var db = {
		makeQuery: function(qtylimit,tblname) {
			this.initialize();
    		var weeklyTbl = Parse.Object.extend(tblname);
    		var query = new Parse.Query(weeklyTbl);
    		query.limit = qtylimit;
    		query.descending('sno');
    		return query;
		},
		filterQuery: function(fieldname, fieldvalue,tblname) {
			this.initialize();
    		var weeklyTbl = Parse.Object.extend(tblname);
    		var query = new Parse.Query(weeklyTbl);
    		query.equalTo(fieldname, fieldvalue);
    		return query;
		},
		weeklyTeachs: function(qtylimit) {
    		var query = this.makeQuery(qtylimit, "weekly");
    		query.find({
	    		  success: function(results) {
	    			  wp.udpateWeeklyView(results);
	    		  },
	    		  error: function(error) {
	    		    alert("Error when retrieving weekly teachings, load from parsedb: " + error.code + " " + error.message);
	    		  }
	    		});
		},
		dailyTeachs: function(qtylimit) {
    		var query = this.makeQuery(qtylimit, "daily");
    		query.find({
	    		  success: function(results) {
	    			  wp.updateDailyView(results);
	    		  },
	    		  error: function(error) {
	    		    alert("Error when retrieving daily teachings, load from parsedb: " + error.code + " " + error.message);
	    		    //use local json data and render page.
	    		  }
	    		});
		},
		expTeachs: function(qtylimit) {
    		var query = this.makeQuery(qtylimit, "experiences");
    		query.find({
	    		  success: function(results) {
	    			  wp.updateExpView(results);
	    		  },
	    		  error: function(error) {
	    		    alert("Error when retrieving experiences, load from parsedb: " + error.code + " " + error.message);
	    		    //use local json data and render page.
	    		  }
	    		});
		},
		confTeachs: function(qtylimit) {
    		var query = this.makeQuery(qtylimit, "broadcasts");
    		query.find({
	    		  success: function(results) {
	    			  wp.updateConfView(results);
	    		  },
	    		  error: function(error) {
	    		    alert("Error when retrieving broadcasts/conferences, load from parsedb: " + error.code + " " + error.message);
	    		    //use local json data and render page.
	    		  }
	    		});
		},
		singleExpTeachs: function(objectId) {
    		var query = this.filterQuery("objectId",objectId, "experiences");
    		query.find({
	    		  success: function(results) {
	    			  wp.updateExpView(results);
	    		  },
	    		  error: function(error) {
	    		    alert("Error when retrieving particular experiences, load from parsedb: " + error.code + " " + error.message);
	    		    //use local json data and render page.
	    		  }
	    		});
		},
		initialize: function() {
	        Parse.initialize("XH6NheOL49WCBRnxYOqeVxziAOpjs3w26K4Pzm2i", "SRTJtgANylqagrQUaKd7CTDHCxVdLDGUzP0pflOR");
	    },
};


var wp = {
		
		udpateWeeklyView: function(results){
	    	var wtdata;
    		var wtsource = $("#wtsTemplate").html(); 
    		var wtTemplate = Handlebars.compile(wtsource); 
    		
    		 wtdata = {"results":results};
			 $("#wtsHolder").append(wtTemplate(wtdata));
	    },
	    
	    updateDailyView: function(results){
	    	
	    	var dtdata = {"results":results};
	    	//$("#dtsHolder").append(    Handlebars.templates["daily"](dtdata));
	    	
	    	var dtsource = $("#dtsTemplate").html(); 
    		var dtTemplate = Handlebars.compile(dtsource); 
			 $("#dtsHolder").append(dtTemplate(dtdata));
	    },
	    
	    updateExpView: function(results){
	    	
	    	var expdata = {"results":results};
	    	//$("#dtsHolder").append(    Handlebars.templates["daily"](dtdata));
	    	
	    	var expsource = $("#expTemplate").html(); 
    		var expTemplate = Handlebars.compile(expsource); 
			 $("#expHolder").append(expTemplate(expdata));
	    },
	    
	    updateConfView: function(results){
	    	
	    	var confdata = {"results":results};
	    	//$("#dtsHolder").append(    Handlebars.templates["daily"](dtdata));
	    	
	    	var confsource = $("#confTemplate").html(); 
    		var confTemplate = Handlebars.compile(confsource); 
			 $("#confHolder").append(confTemplate(confdata));
	    },
	    
	    initializeHandleBars: function (){
	    	Handlebars.registerHelper('iflastColumn', function(sno) {
	    		if(!(sno%2))
	    			return 'last-column';
	    		});

	    	Handlebars.registerHelper('ifHideResponsive', function(sno) {
	    		if(sno%2)
	    		  return 'hide-if-responsive';
	    		});
	    	
	    	Handlebars.registerHelper('getDate', function(dateObj) {
	    		var options = { month: "short", day: "numeric", year: "numeric"};
	    			return new Intl.DateTimeFormat("en-GB",options).format(dateObj);//.toLocaleTimeString();
	    		});
	    	
	    	Handlebars.registerHelper('getMonthinDate', function(dateObj) {
	    		var options = {month: "short"};
	    			return new Intl.DateTimeFormat("en-GB",options).format(dateObj);//.toLocaleTimeString();
	    		});

	    	Handlebars.registerHelper('getDayinDate', function(dateObj) {
	    		var options = {day: "2-digit"};
	    			return new Intl.DateTimeFormat("en-GB",options).format(dateObj);//.toLocaleTimeString();
	    		});
	    }
};



$( document ).ready(function() {

app.initialize();

});



